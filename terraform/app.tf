# Больше примеров развёртывания инстансов можно увидеть в ридми к модулю
# https://github.com/realscorp/tf-openstack-vkcs-vm

# # Windows-инстанс сервера приложения
#module "m-app-example" {
#  source       = "git::https://gitlab.ap.com/iac/lib/tf-openstack-vkcs-vm.git"
#  name         = "app-1.demo"
#  flavor       = var.app_flavor
#  image        = "cirros-0.6.1"
#  region       = "DEMO-01"
#  az           = "nova"
#  ssh_key_name = openstack_compute_keypair_v2.terraform.name # Не изменять
#  metadata = {
#    env    = "dev"
#  }
#  ports = [
#    {
#      network             = "int-network"
#      subnet              = "int-subnet-1"
#      ip_address          = "10.10.1.30"
#      dns_record          = true
#      dns_zone            = var.dns_zone # Не изменять
#      security_groups     = var.app_sg
#      security_groups_ids = []
#    }
#  ]
#  volumes = {
#    root = {
#      type = "__DEFAULT__"
#      size = var.app_root_vol_size
#    }
#  }
#}