#######################
# Переменные для работы провайдера dns, по-умолчанию заданы значения для зоны test.example.com
#######################
variable "dns_username" {
  description = "Default value gives access to ap.com. zone"
  type        = string
  default     = "dns-tech"
}
variable "dns_password" {
  description = "Default value gives access to ap.com. zone"
  type        = string
  default     = "xxxxxxxxxxxxxxxxxxx"
}
variable "dns_zone" {
  description = "Default zone to create DNS records"
  type        = string
}

#######################
# Блок переменных для предоставленных примеров
# Сами значения для работы в песочнице задаются в playground.auto.tfvars,
# значения для боевых окружений - в каталоге environment/
#######################

# APP
########
 variable "app_flavor" {
   description = "APP Server flavor"
   type        = string
 }
 variable "app_sg" {
   description = "APP server SGs"
   type        = list(string)
 }
 variable "app_root_vol_size" {
   description = "APP root volume size"
   type        = number
 }
