terraform {
  required_providers {
    openstack = {
      source  = "terraform-provider-openstack/openstack"
      version = "~> 1.54.1"
    }
  }
}

provider "dns" {
  update {
    server = "ad.ap.com"
    gssapi {
      realm    = "AP.COM"
      username = var.dns_username
      password = var.dns_password
    }
  }
}
